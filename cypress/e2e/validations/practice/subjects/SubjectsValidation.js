import { Utils } from "../../../../utils/Utils"
import { SubjectConstants } from "../constants/SubjectConstants"

export class SubjectValidation {
    selectOneSubject() {
        let randomNumber = Utils.getRandomInt(0,3)
        return cy.get(SubjectConstants.SUBJECT_CARDS).eq(randomNumber).click().find("h6").invoke("text")
    }
}