import { TestResultScreenConstants } from "../constants/TestResultScreenConstants";

export class TestResultScreenValidation {
    validateScore(correctQuestionCount, totalQuestionCount) {
        cy.get(TestResultScreenConstants.SCORE).then(($score) => {
            let [extractedCorrectQuestionCount, extractedTotalQuestionCount] = $score.text().split("/")
            expect(parseInt(extractedCorrectQuestionCount)).to.be.equal(correctQuestionCount)
            expect(parseInt(extractedTotalQuestionCount)).to.be.equal(totalQuestionCount)
        })
    }

    validateTotalTimeTakenChart(totalTimeTaken) {
        cy.xpath(TestResultScreenConstants.TIME_CHART_VALUE).then(($totalTimeTaken) => {
            expect($totalTimeTaken.text()).to.be.equal(totalTimeTaken)
        })
    }
}