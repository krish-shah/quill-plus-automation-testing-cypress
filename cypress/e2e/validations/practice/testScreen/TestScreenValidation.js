/// <reference types="Cypress" />

import { TestScreenConstants } from "../constants/TestScreenConstants"
import { Utils } from '../../../../utils/Utils'

export class TestScreenValidation {
    isCheckAnswerRouteIntercepted = false
    correctQuestionCount = 0
    incorrectQuestionCount = 0
    totalQuestions = 0
    totalTimeTaken = ""

    setTotalQuestions() {
        cy.get(TestScreenConstants.QUESTION_PILLS).then(($questionPills) => {
            this.totalQuestions = $questionPills.length
        })
    }

    getTotalQuestionCount() {
        return this.totalQuestions
    }

    getIncorrectQuestionCount() {
        return this.incorrectQuestionCount
    }

    getCorrectQuestionCount() {
        return this.correctQuestionCount
    }

    getTotalTimeTaken() {
        return this.totalTimeTaken
    }

    incrementCorrectQuestionCount() {
        this.correctQuestionCount++
    }

    incrementIncorrectQuestionCount() {
        this.incorrectQuestionCount++
    }

    interceptCheckAnswerRoute() {
        cy.intercept(TestScreenConstants.METHOD_POST, TestScreenConstants.CHECK_ANSWER_ROUTE).as(TestScreenConstants.CHECK_ANSWER_ROUTE_ALIAS)
    }

    selectRandomOption() {
        let randomNumber = Utils.getRandomInt(0, 3)
        return cy.get(TestScreenConstants.OPTIONS).eq(randomNumber).click()
    }

    clickOnSubmitButton() {
        cy.get(TestScreenConstants.SUBMIT_BUTTON).click()
    }

    clickOnConfirmSubmit() {
        cy.get(TestScreenConstants.CONFIRM_SUBMIT_BUTTON).click()
    }

    clickOnNextButton() {
        cy.get(TestScreenConstants.NEXT_BUTTON).click()
    }

    setTotalTimeTaken() {
        cy.get(TestScreenConstants.TIMER).then(($timer) => {
            let [hours, minutes, seconds] = $timer.text().split(":")
            if(minutes === "00") {
                this.totalTimeTaken = seconds.replace("/^0+/", "") + "s"
            } else {
                this.totalTimeTaken = minutes.replace("/^0+/", "") + "m" + " : " + seconds.replace("/^0+/", "") + "s"                
            }
        })
    }

    attemptTest() {
        if(!this.isCheckAnswerRouteIntercepted) {
            this.interceptCheckAnswerRoute()
            this.setTotalQuestions()
            this.isCheckAnswerRouteIntercepted = true
        }

        this.selectRandomOption().then(($selectedOption) => {
            this.validateCorrectIncorrectAnswer($selectedOption)
        })

        cy.get(TestScreenConstants.BOTTOM_NAV_HOLDER).then(($navigationHolder) => {
            if($navigationHolder.find(TestScreenConstants.NEXT_BUTTON).length === 0) {
                this.clickOnSubmitButton()
                this.clickOnConfirmSubmit()
                this.setTotalTimeTaken()
            } else {
                this.clickOnNextButton()
                this.attemptTest()
            }
        })
    }

    validateCorrectIncorrectAnswer($selectedOption) {
        cy.wait("@"+TestScreenConstants.CHECK_ANSWER_ROUTE_ALIAS).then((xhr) => {
            if(xhr.response.body.data.was_correct === false) {
                cy.wrap($selectedOption).should("have.class", TestScreenConstants.INCORRECT_ANSWER_CLASS)
                cy.get(TestScreenConstants.OPTIONS).should("have.class", TestScreenConstants.CORRECT_ANSWER_CLASS)
                this.incrementIncorrectQuestionCount()
            } else {
                cy.wrap($selectedOption).should("have.class", TestScreenConstants.CORRECT_ANSWER_CLASS)
                this.incrementCorrectQuestionCount()
            }
        })
    }

    waitForTestToSubmit() {
        cy.contains(TestScreenConstants.SUBMITTING_TEST_TEXT).should("be.visible")
        return cy.contains(TestScreenConstants.SUBMITTING_TEST_TEXT).should("not.be.visible")
    }
}