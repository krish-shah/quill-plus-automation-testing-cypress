/// <reference types="Cypress" />
import { LoginConstants } from "./constants/LoginConstants";

export class LoginValidation {
    visitLoginPage() {
        cy.visit(LoginConstants.LOGIN_URL)
    }

    enterPhone(phoneNumber) {
        cy.get(LoginConstants.PHONE_INPUT).type(phoneNumber)
    }

    clickOnProceedButton() {
        cy.contains(LoginConstants.PROCEED_TEXT).click();
    }

    enterOTP() {
        cy.get(LoginConstants.OTP_INPUT).type(LoginConstants.DEFAULT_OTP, {delay:500})
    }

    interceptIsUserRegisteredRequest() {
        cy.intercept("POST", LoginConstants.IS_USER_REGISTERED_URL).as("isUserRegisteredRequest")
    }

    validateIsUserRegisteredResponse() {
        cy.wait("@isUserRegisteredRequest").then((xhr) => {
            expect(xhr.response.statusCode).to.be.equal(LoginConstants.STATUS_CODE_404)
            expect(xhr.response.body.message).to.be.equal(LoginConstants.USER_DOES_NOT_EXIST_MESSAGE)
        })
    }

    interceptVerifyOTPRequest() {
        cy.intercept("POST", LoginConstants.VERIFY_OTP_URL, {
            statusCode: 400,
            body: {
                "message" : "Bad Request",
                "data" : {
                    "error" : "invalid otp given for phone number"
                }
            }
        }).as("verifyOTPRequest")
    }

    validateWrongOTPMessage() {
        cy.wait("@verifyOTPRequest")
        cy.contains("invalid otp given for phone number").should("be.visible")
    }
}