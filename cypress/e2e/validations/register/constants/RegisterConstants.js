export class RegisterConstants {
    static NAME = "#name"
    static EMAIL = "#email"
    static DOB = "#date_of_birth"
    static CALENDAR_YEAR = ".numInput.cur-year"
    static CALENDAR_MONTH = ".flatpickr-monthDropdown-months"
    static GENDER_PARENT_DIV = "//label[normalize-space()='Gender']/parent::div/div/div"
    static STATE_PARENT_DIV = "//label[normalize-space()='State']/parent::div/div/div"
    static CITY_PARENT_DIV = "//label[normalize-space()='City']/parent::div/div/div"
    static NEXT_BUTTON_TEXT = "Next"
    static FETCH_CITIES_ROUTE = "**/internal/state/*/cities/"
}