import { LoginTests } from "../../tests/login/LoginTests";

const loginTests = new LoginTests()
Given("I visited login page", () => {
    loginTests.visitLoginPage()
})

Given("I logged in with {string} and default OTP", (phoneNumber) => {
    loginTests.loginWithValidCredentials(phoneNumber)
})

Given("I tried to log in with wrong OTP and error message was visible", () => {
    loginTests.loginWithWrongOTPAndValidateErrorMessage()
})