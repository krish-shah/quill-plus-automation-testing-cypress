import { RegisterTests } from "../../tests/register/RegisterTests";

const registerTests = new RegisterTests()

Given("I logged in with random number and default OTP", () => {
    registerTests.loginWithRandomPhoneNumber()
})

Then("I filled the registration form", (datatable) => {
    registerTests.fillRegistrationDetails(datatable)
}) 