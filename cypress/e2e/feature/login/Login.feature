Feature: User login flow
    Background: User is on login page
        Given I visited login page

    Scenario: User logged in with valid credentials
        Given I logged in with "9930995548" and default OTP
    
    Scenario: wrong OTP message
        Given I tried to log in with wrong OTP and error message was visible