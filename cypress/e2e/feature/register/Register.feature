Feature: User registration flow
    Background: User is on login page
        Given I visited login page

    Scenario: User registration
        Given I logged in with random number and default OTP
        Then I filled the registration form
        |name|email|dob|gender|state|city|
        |krish|random|2003-11-12|male|Maharashtra|Mumbai|